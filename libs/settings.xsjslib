var Settings = {
    docTableName: '"Konovalov.hana_srdl"."Konovalov.hana_srdl::hana_srdl.tDoc"',
    mobileTableName: '"Konovalov.hana_srdl"."Konovalov.hana_srdl::hana_srdl.tMobileSync"',
    reportTypeTableName: '"Konovalov.hana_srdl"."Konovalov.hana_srdl::hana_srdl.tReport"',
    searchKeyTableName: '"Konovalov.hana_srdl"."Konovalov.hana_srdl::hana_srdl.tSearchKey"',
    view1: '"Konovalov.hana_srdl"."Konovalov.hana_srdl::hana_srdl.Words"',
    docFullTextIndex: '"Konovalov.hana_srdl"."$TA_FULLTEXT_SPEECH_RDL_DOC"',
    docNameLimit: 240
}