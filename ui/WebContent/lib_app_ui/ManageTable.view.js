//To use a javascript view its name must end with .view.js
sap.ui.jsview("lib_app_ui.ManageTable", {

    getControllerName : function() {
        return "lib_app_ui.ManageTable";
    },
    
    createContent : function(oController) {
        for (var obj in manageTables)
            oController.createTableLayout(obj);        

        return [];
    }
});
