//To use a javascript view its name must end with .view.js
sap.ui.jsview("lib_app_ui.UploadDoc", {

    getControllerName : function() {
        return "lib_app_ui.UploadDoc";
    },
    
    createContent : function(oController) {
        var oLayout = new sap.ui.commons.layout.MatrixLayout({layoutFixed : false,width:"100%",height:"100%"});
        var oFileUploader = new sap.ui.commons.FileUploader({
          name: "uploader_",
          width: "40%",
          uploadOnChange: false,
          uploadUrl: addDocPath,
          uploadComplete: function (oEvent) {
            var sResponse = oEvent.getParameter("response");
              if (sResponse) {
                // var m = /^\[(\d\d\d)\]:(.*)$/.exec(sResponse);
                if (sResponse == "200") {
                  setNotificationMessage('Документ загружен');
                        // sap.ui.commons.MessageBox.show("Return Code: " + m[1] + "\n" + m[2], "SUCCESS", "Upload Success");
                } else {
                  setNotificationMessage('Не удалось загрузить документ');
                        // sap.ui.commons.MessageBox.show("Return Code: " + m[1] + "\n" + m[2], "ERROR", "Upload Error");
                }                
              }
          }});        

        var oUploadBtn = new sap.ui.commons.Button({
          text:'Загрузить',
          press:function() {
            clearNotificationMessage();
            oFileUploader.upload();
          }
        }).addStyleClass("fontSize24");        

        oLayout.createRow(oFileUploader);
        oLayout.createRow(oUploadBtn);
        return oLayout;
    }
});
