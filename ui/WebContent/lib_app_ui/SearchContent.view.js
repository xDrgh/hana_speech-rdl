//To use a javascript view its name must end with .view.js
sap.ui.jsview("lib_app_ui.SearchContent", {

    getControllerName : function() {
        return "lib_app_ui.SearchContent";
    },
    
    createContent : function(oController) {
        var searchDocName; 
        var searchType = '';
        var searchDocUseFuzzy = true;
        var oLayout = new sap.ui.commons.layout.MatrixLayout({layoutFixed : false,width:"100%",height:"100%"});
        var jModel = new sap.ui.model.json.JSONModel();
        var jModel2 = new sap.ui.model.json.JSONModel();  //для списка токенов-ключей
        //заменить на голосовой поиск
        var oTFSearch = new sap.ui.commons.TextField({id: "idTFSearch", width: "500px", value:"поиск документа"}).addStyleClass("fontSize24");
        // oTFSearch.attachChange(function(){
        oTFSearch.attachLiveChange(function(oEvent){     
          var newValue = oEvent.getParameter("liveValue");
          if (!newValue) return;
          var url = searchDocPath+'?s='+newValue;
          if (!searchDocUseFuzzy)
            url+='&exact=1';
          jModel.loadData(url, {}, true, "GET");
          oTable.clearSelection();
          // oComboBox.setValue();
          searchDocName = '';
          jModel2.setData({});
        });
        var oSearchDocCB = new sap.ui.commons.CheckBox({
          text : 'Нечеткий поиск',
          tooltip : 'При поставленой галочке используется нечеткий поиск, иначе точное сопоставление',
          checked : true,
          change : function() {
            oSearchDocCB.getChecked() ? searchDocUseFuzzy = true : searchDocUseFuzzy = false;

          var newValue = oTFSearch.getValue();
          if (!newValue) return;
          var url = searchDocPath+'?s='+newValue;
          if (!searchDocUseFuzzy)
            url+='&exact=1';
          jModel.loadData(url, {}, true, "GET");
          oTable.setSelectedIndex();
          // oComboBox.setValue();
          }
        }).addStyleClass("fontSize24");

        var oTable = new sap.ui.table.Table({
          visibleRowCount: 5,
          editable: false,
          width: "100%",          
          title: "Результаты поиска",
//            height: '100%',
          selectionBehavior: sap.ui.table.SelectionBehavior.Row
        });

        oTable.addColumn(new sap.ui.table.Column({
          label: new sap.ui.commons.Label({text: "Название документа"}),
          template: new sap.ui.commons.TextField().bindProperty("value", "name"),
          sortProperty: "name",
          filterProperty: "name",
          width: "30%"
        }));
        oTable.addColumn(new sap.ui.table.Column({
          label: new sap.ui.commons.Label({text: "Контекст"}),
          template: new sap.ui.commons.TextField().bindProperty("value", "context"),
          width: "70%"
        }));
        oTable.bindRows("/result");
        oTable.setModel(jModel);

        oTable.attachRowSelectionChange(function(oEvent){
          if (oEvent.oSource.getProperty("selectedIndex")==-1) {  //снятие выделения
            // searchDocName = '';
            // jModel.setData({});
          }
          else {
            var path = oEvent.getParameter("rowContext").sPath;
            var name = jModel.getProperty(path).name;      
            searchDocName = name;    
            oController.updateTextAnalysTable(searchDocName,searchType,jModel2);  
            // oComboBox.setValue();
            // jModel2.setData({});
            // console.log(id,path);
          }
          
        });

        var oTableToolbar = new sap.ui.commons.Toolbar({items: [
          new sap.ui.commons.Button({
            text:'Скачать документ',
            press:function() {
              if (!searchDocName) return;
              var url = downloadDocPath+'?name='+searchDocName;
              jQuery.download(url, {}, 'post');
            }
          })
        ]});        

        oTable.setToolbar(oTableToolbar);

        var oLabel = new sap.ui.commons.Label({text: "Категория"}).addStyleClass("fontSize24");

        var oTaTYPEModel = new sap.ui.model.json.JSONModel();
        oTaTYPEModel.setData([
          {name:"PRODUCT", text:"Продукт"}, 
          {name:"PERSON", text:"Персона"},
          {name:"TITLE", text:"Заголовки"},

          {name:"ADDRESS1", text:"Адрес 1"},
          {name:"ADDRESS2", text:"Адрес 2"},
          
          {name:"ORGANIZATION/COMMERCIAL", text:"Коммерческие организации"},
          {name:"ORGANIZATION/SPORTS", text:"Спортивные организации"},
          {name:"ORGANIZATION/GOVERNMENT", text:"Государственное учреждение"},
          {name:"ORGANIZATION/MEDIA", text:"ORGANIZATION/MEDIA"},
          {name:"ORGANIZATION/EDUCATIONAL", text:"Образовательное учреждение"},
          {name:"ORGANIZATION/OTHER", text:"Организации (другие)"},
          {name:"URI/URL", text:"URI/URL"},
          {name:"URI/EMAIL", text:"URI/EMAIL"},
          {name:"PHONE", text:"Телефон"},

          {name:"LOCALITY", text:"Населенные пункты"},         
          {name:"GEO_FEATURE", text:"GEO_FEATURE"},
          {name:"REGION/MAJOR", text:"REGION/MAJOR"},          
          {name:"CONTINENT", text:"Континенты"},
          {name:"GEO_AREA", text:"Регионы"},
          {name:"COUNTRY", text:"Страны"},
          
          {name:"LANGUAGE", text:"Язык"},
          {name:"MEASURE", text:"Показатели"},
          {name:"PERCENT", text:"Проценты"},
          
          {name:"CURRENCY", text:"Валюты"},
          {name:"FACILITY/BUILDGROUNDS", text:"FACILITY/BUILDGROUNDS"},
          {name:"PEOPLE", text:"Нации"},
          
          {name:"DATE", text:"Дата"},
          {name:"TIME_PERIOD", text:"Временной период"},
          {name:"TIME", text:"Время"},
          {name:"YEAR", text:"Год"},
          {name:"MONTH", text:"Месяц"},
          {name:"DAY", text:"День"},
          {name:"NOUN_GROUP", text:"Именная группа"},
          {name:"PROP_MISC", text:"PROP_MISC"},
        ]);

        // Create a ComboBox
        var oComboBox = new sap.ui.commons.ComboBox({width:"auto"}).addStyleClass("fontSize24");
        oComboBox.setModel(oTaTYPEModel);
        var oDocSearchItemTemplate = new sap.ui.core.ListItem();
        oDocSearchItemTemplate.bindProperty("text", "text");
        oDocSearchItemTemplate.bindProperty("tooltip", "name");
        // oDocSearchItemTemplate.addStyleClass("fontSize24");
        oComboBox.bindItems("/", oDocSearchItemTemplate);

        
        oComboBox.attachChange(function(oEvent){
          searchType = oEvent.getParameter("selectedItem").getAggregation("tooltip");
          oController.updateTextAnalysTable(searchDocName,searchType,jModel2);
        });
        oComboBox.setSelectedItemId(oComboBox.getItems()[0].sId);
        searchType = oComboBox.getItems()[0].getAggregation("tooltip");
            

        var oTable2 = new sap.ui.table.Table({
          visibleRowCount: 10,
          editable: false,
          width: "100%",          
          title: "Результаты анализа",
//            height: '100%',
          selectionBehavior: sap.ui.table.SelectionBehavior.Row
        });

        oTable2.addColumn(new sap.ui.table.Column({
          // label: new sap.ui.commons.Label({text: "Токен"}),
          template: new sap.ui.commons.TextField().bindProperty("value", "word"),
          sortProperty: "word",
          filterProperty: "word",
          width: "30%"
        }));
        oTable2.bindRows("/result");
        oTable2.setModel(jModel2);

        var oHLayout = new sap.ui.layout.HorizontalLayout({
          content: [oTFSearch, oSearchDocCB]
        });
        var oHLayout2 = new sap.ui.layout.HorizontalLayout({
          content: [oLabel, oComboBox]
        });

        oLayout.createRow(oHLayout);
        oLayout.createRow(oTable);
        oLayout.createRow(oHLayout2);
        oLayout.createRow(oTable2);
        return oLayout;
    }
});
