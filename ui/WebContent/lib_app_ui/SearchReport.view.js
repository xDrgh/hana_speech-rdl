sap.ui.jsview("lib_app_ui.SearchReport", {

    getControllerName : function() {
        return "lib_app_ui.SearchReport";
    },
    
    createContent : function(oController) {
        //поиск и открытие отчета          
        var oLayout = new sap.ui.commons.layout.MatrixLayout({layoutFixed : true,width:"100%",height:"100%",widths:['85%','15%']});
        var oLabel = new sap.ui.commons.TextView({width:"100%",height:"100%"});
        oLabel.bindElement("/0");
        oLabel.bindProperty("text", "text");
        oLabel.addStyleClass('idBigStatusText');
        var oImage = new sap.ui.commons.Image({width:"100%"});
        oImage.setSrc("icons/mic2.png");
        oImage.setTooltip("Нажмите, чтобы начать/остановить распознование");
        oImage.setDecorative(false);
        oImage.attachPress(toogleRecognizing);

        oLayout.createRow(oLabel,oImage);

        return oLayout;
    }
});
