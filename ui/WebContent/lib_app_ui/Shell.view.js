sap.ui.jsview("lib_app_ui.Shell", {

    getControllerName : function() {
        return "lib_app_ui.Shell";
    },

    createContent : function(oController) {           
      
//-----------------------        инициализация    

        //поиск и открытие отчета 
        var oSearchReportView = sap.ui.view({id:"idSearchReportView", viewName:"lib_app_ui.SearchReport", type:sap.ui.core.mvc.ViewType.JS}); 

        //добваление отчета, голосовой команды для отчета
        var oAddReportView = sap.ui.view({id:"idAddReportView", viewName:"lib_app_ui.AddReport", type:sap.ui.core.mvc.ViewType.JS});  

        //поиск и анализ документа
        var oSearchContentView = sap.ui.view({id:"idSearchContentView", viewName:"lib_app_ui.SearchContent", type:sap.ui.core.mvc.ViewType.JS});

        //загрузка пдф
        var oUploadDocView = sap.ui.view({id:"idUploadDocView", viewName:"lib_app_ui.UploadDoc", type:sap.ui.core.mvc.ViewType.JS});
        
        //удаление записей
        var oManageTableView = sap.ui.view({id:"idManageTableView", viewName:"lib_app_ui.ManageTable", type:sap.ui.core.mvc.ViewType.JS});

        var oShell = new sap.ui.ux3.Shell("myShell",{   
            appTitle: "Голосовой запрос к HANA",
//            appIcon : "http://hana21:8010/libindicators/LIM_PHD/SNG_logo2.jpg",            
            // headerType : sap.ui.ux3.ShellHeaderType.NoNavigation,
            showSearchTool: false,
            showFeederTool : false,
            showPane : false,
            allowOverlayHeaderAccess : true,
            content: oSearchReportView,
            worksetItems: [
              new sap.ui.ux3.NavigationItem("WI_REPORT",{text:"Поиск отчета"}),
              new sap.ui.ux3.NavigationItem("WI_ADD_REPORT",{text:"Добавление команд"}),
              new sap.ui.ux3.NavigationItem("WI_DOC_SEARCH",{text:"Поиск по содержанию"}),
              new sap.ui.ux3.NavigationItem("WI_DOC_LOAD",{text:"Загрузка документа"}),
              new sap.ui.ux3.NavigationItem("WI_MANAGE_TAB",{text:"Управление записями",subItems:[
                new sap.ui.ux3.NavigationItem("WI_M_1",{text:"Отчеты"}),
                new sap.ui.ux3.NavigationItem("WI_M_2",{text:"Голосовые команды"}),
                new sap.ui.ux3.NavigationItem("WI_M_3",{text:"Документы"})
              ]}),              
            ],

            worksetItemSelected: function(oEvent){
              var sId = oEvent.getParameter("id");
              var oShell = oEvent.oSource;
              switch (sId) {
                case "WI_REPORT":
                  oShell.setContent(oSearchReportView);
                  useStatusBar = false;
                  clearNotificationMessage();
                  gPage = 1;
                  break;
                case "WI_ADD_REPORT":
                  oShell.setContent(oAddReportView);
                  useStatusBar = true;
                  clearNotificationMessage();
                  gPage = 2;
                  break;
                case "WI_DOC_SEARCH":
                  oShell.setContent(oSearchContentView);
                  useStatusBar = true;
                  clearNotificationMessage();
                  gPage = 3;
                  break;
                case "WI_DOC_LOAD":
                  oShell.setContent(oUploadDocView);
                  useStatusBar = true;
                  clearNotificationMessage();
                  gPage = 4;
                  break;
                case "WI_MANAGE_TAB":
                  oShell.setContent(oManageTableView.oController.oLayoutsCol['REPORTTYPE']);
                  useStatusBar = true;
                  clearNotificationMessage();
                  gPage = 5;
                  break;
                case "WI_M_1":
                  oShell.setContent(oManageTableView.oController.oLayoutsCol['REPORTTYPE']);
                  useStatusBar = true;
                  clearNotificationMessage();
                  gPage = 5;
                  break;
                case "WI_M_2":
                  oShell.setContent(oManageTableView.oController.oLayoutsCol['SEARCHKEY']);
                  useStatusBar = true;
                  clearNotificationMessage();
                  gPage = 5;
                  break;
                case "WI_M_3":
                  oShell.setContent(oManageTableView.oController.oLayoutsCol['DOC']);
                  useStatusBar = true;
                  clearNotificationMessage();
                  gPage = 5;
                  break;
                default:
                  break;
              }
            },
            showTools : false,
            showLogoutButton : false
        });


    

        jQuery.sap.require("sap.ui.core.IconPool");  
        var oIconVoice = new sap.ui.core.Icon({  
          // src : '../icons/mic.gif',  
          src : sap.ui.core.IconPool.getIconURI('microphone'),   
          size : "18px",  
          color : "white",  
          activeColor : "#333333",  
          activeBackgroundColor : "#333333",  
          hoverColor : "#eeeeee",  
          hoverBackgroundColor : "#666666",  
          tooltip: 'включить/отключить голосовое распознование',
          // width : "36px",  
          press: toogleRecognizing
        });

        // // var userNameText = new sap.ui.commons.TextView({text:oController.getUserName(),tooltip:"U.Name"});

        var oLayoutHeader = new sap.ui.layout.HorizontalLayout({
            content: [oIconVoice]//,oIconAddRep]//,userNameText]
        });
        oLayoutHeader.addStyleClass("verticalAlignTop");
        oShell.addHeaderItem(oLayoutHeader);

        function displayListener(oEvent) {
          var bShow = oEvent.getParameter("show");

          if (bShow) {
            /*
             * Now the application can decide how to display the bar. It can be maximized, default, minimized (please see NotificationBarStatus) 
             */
            var sStatus = sap.ui.ux3.NotificationBarStatus.Default;
            oNotificationBar.setVisibleStatus(sStatus);
          } else {
            var sStatus = sap.ui.ux3.NotificationBarStatus.None;
            oNotificationBar.setVisibleStatus(sStatus);
          }
        };

        var oNotifier = new sap.ui.ux3.Notifier();
        gNotifier = oNotifier;

        var oNotificationBar = new sap.ui.ux3.NotificationBar({
          display : displayListener,
          visibleStatus : "None",
          resizeEnabled : false
        });
        oNotificationBar.setMessageNotifier(oNotifier);

        // oToolP.setPosition(sap.ui.core.Popup.Dock.RightTop,sap.ui.core.Popup.Dock.LeftBottom, oIconAddRep, "-13 0");

        oShell.setNotificationBar(oNotificationBar);
        return oShell;
    
    }
});
