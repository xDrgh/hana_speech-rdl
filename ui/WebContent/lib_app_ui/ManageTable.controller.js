sap.ui.controller("lib_app_ui.ManageTable", {
	oTablesCol : {},		//коллекция содержит таблицы, доступ через ключи
	oModelsCol : {},		//коллекция содержит модели, доступ через ключи
	oLayoutsCol : {},		//коллекция содержит слои с таблицами
	
	getColumn: function(settings){
		var collection = {
          label: new sap.ui.commons.Label({text: settings.label}),
          template: new sap.ui.commons.TextField().bindProperty("value", settings.bindValue),
          visible: true,
          width: settings.width
        };
        if (settings.useFilter)
        	collection.filterProperty = settings.bindValue;
        if (settings.useSort)
        	collection.sortProperty = settings.bindValue;

		    var column = new sap.ui.table.Column(collection);
        
        return column;
	},

	createTableLayout: function(key){
		var ModelSelectedRow;
		var oModel = new sap.ui.model.odata.ODataModel(oDataPath,true);
		oModel.setDefaultBindingMode(sap.ui.model.BindingMode.TwoWay);
		gTemp = oModel;
		var tableSettings = manageTables[key];

    var oLayout = new sap.ui.commons.layout.MatrixLayout({layoutFixed : false,width:"100%",height:"100%",visible:true});


    var oTable = new sap.ui.table.Table({
      visibleRowCount: 10,
      editable: true,
      width: "100%",    
      title: tableSettings.title,   
      selectionMode: sap.ui.table.SelectionMode.Single,
      selectionBehavior: sap.ui.table.SelectionBehavior.Row
    });
    
    var columns = tableSettings.columns;
    for (var i=0;i<columns.length;i++)
    	oTable.addColumn(this.getColumn(columns[i]));

    oTable.attachRowSelectionChange(function(oEvent){
      if (oEvent.oSource.getProperty("selectedIndex")==-1) {  //снятие выделения
        ModelSelectedRow = undefined;
      }
      else {
        ModelSelectedRow = oEvent.getParameter("rowContext").sPath;
      }
      
    });
    
    
    oTable.setModel(oModel);
    oTable.bindRows({
        path: tableSettings.bind,
        parameters: tableSettings.bindParameters
    });


    // oTable.bindRows(tableSettings.bind);

    var oTableToolbar = new sap.ui.commons.Toolbar({items: [ 
        new sap.ui.commons.Button({
          text:'Удалить',
          press:function(oEvent) {
            clearNotificationMessage();
            if (ModelSelectedRow){                  
              sap.ui.commons.MessageBox.confirm("Вы действительно хотите удалить запись?", 
                function(result){if (result) oModel.remove(ModelSelectedRow);oTable.clearSelection();}, 
                "Подтверждение");              
            }
            else {
              setNotificationMessage("Не выбраны данные для удаления");
            }                
          }
        }),
        new sap.ui.commons.Button({
          text:'Подтвердить изменения',
          press:function(oEvent) {
            clearNotificationMessage();
            if (oModel.sChangeKey) {
              var a = oModel.sChangeKey.replace(oModel.sServiceUrl, '');
              var p = oModel._getObject(a);

              var oEntry = {};
              for (var obj in p)
                if (typeof(p[obj])!="object") //бинарный тип не поддерживает
                  oEntry[obj]=p[obj];
             
              var oParams = {};
              oParams.fnSuccess = function(){setNotificationMessage("Данные успешно обновлены");};
              oParams.fnError = function(){setNotificationMessage("Не удалось обновить данные");};
              oParams.bMerge = false;     //PUT
              oModel.update(a, oEntry, oParams);
            }

            // Merge
            // oModel.submitChanges(  
            //   function(){setNotificationMessage("Данные успешно обновлены");},
            //   function(){setNotificationMessage("Не удалось обновить данные");}
            // );

            // POST
            // oModel.create(a, {"name":1,"product":2}, null, function(){
            //     alert("Create successful");
            // },function(){
            //     alert("Create failed");});
          }
        }),
        new sap.ui.commons.Button({
          text:'Обновить данные',
          press:function(oEvent) {
            clearNotificationMessage();
            oModel.refresh();
            oTable.clearSelection();
          }
        })
    ]});
    oTable.setToolbar(oTableToolbar);
    oLayout.createRow(oTable);

    this.oTablesCol[key] = oTable;
		this.oModelsCol[key] = oModel;
		this.oLayoutsCol[key] = oLayout;
    return oLayout;
	}

});

