sap.ui.controller("lib_app_ui.Shell", {
	onInit: function(){
		if ('webkitSpeechRecognition' in window)
			startRecognizing();
	},

    getUserName: function(){
        var data = new sap.ui.model.json.JSONModel();
        var url = '../../services/session.xsjs';    
        data.loadData(url, {}, false, "GET");           
        return data.oData.session.UserName;
    }

});