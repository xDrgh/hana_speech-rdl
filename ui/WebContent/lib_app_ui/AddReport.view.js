//To use a javascript view its name must end with .view.js
sap.ui.jsview("lib_app_ui.AddReport", {

    getControllerName : function() {
        return "lib_app_ui.AddReport";
    },
    
    createContent : function(oController) {
        var oLayout = new sap.ui.commons.layout.MatrixLayout({layoutFixed : false,width:"100%",height:"100%",widths:['20%','80%']});
        
        var oRTipName = new sap.ui.commons.RichTooltip({text: 'Обязательное поле. Название поля'});
        var oRTipRef = new sap.ui.commons.RichTooltip({text: 'Обязательное поле. Ссылка на отчет'});
        var oRTipKey = new sap.ui.commons.RichTooltip({text: 'Необязательное поле. Поддерживает несколько значений через ";". При пустом значении - поисковая фраза будет браться из имени очета'});
        var oTFName = new sap.ui.commons.TextField({id:"idTFName", width: "500px", required:true, value:"Название отчета"}).setTooltip(oRTipName).addStyleClass("fontSize24");
        var oTFRef = new sap.ui.commons.TextField({id:"idTFRef", width: "500px", required:true, value:"http://..."}).setTooltip(oRTipRef).addStyleClass("fontSize24");
        var oTFKey = new sap.ui.commons.TextField({id:"idTFSearchWord", width: "500px", required:false, value:""}).setTooltip(oRTipKey).addStyleClass("fontSize24");
        var oAddReportBtn = new sap.ui.commons.Button({ text: "Добавить", 
          press: function(){
            clearNotificationMessage();
            addNewReport(oTFName.getValue(),oTFRef.getValue(),oTFKey.getValue());
            }
        }).addStyleClass("fontSize24");

        // oLayout.createRow(new sap.ui.commons.Label({text : "Добавление отчета"}));
        oLayout.createRow(new sap.ui.commons.Label({text : "Название отчета", labelFor:"idTFName"}).addStyleClass("fontSize24"),oTFName);
        oLayout.createRow(new sap.ui.commons.Label({text : "Ссылка на отчет", labelFor:"idTFRef"}).addStyleClass("fontSize24"),oTFRef);
        oLayout.createRow(new sap.ui.commons.Label({text : "Поисковые фразы", labelFor:"idTFSearchWord"}).addStyleClass("fontSize24"),oTFKey);
        oLayout.createRow(oAddReportBtn);
        return oLayout;
    }
});
