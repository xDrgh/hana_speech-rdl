﻿var gTemp;

var gReportId;
var gParams;
var gNotifier;
var gReport;
var gMainJModel = new sap.ui.model.json.JSONModel();
var useStatusBar = false; //использование statusBar'a для выдачи сообщения
var gPage = 1;      //какая страница выбрана
var gFuncAfterApply;

var oDataPath = '/sap/hana/rdl/odata/v1/Konovalov.hana_srdl/hana_srdl';
var getReportNamePath = '/hana_srdl.getReportName';
var addDoc4MobilePath = '/add2Mobile';  ///hana_srdl.tReport(NAME=<value>)/add2Mobile
var addReportPath = oDataPath+'/hana_srdl.tReport';

var searchDocPath = '../../services/searchDoc.xsjs';
var getDocInfoPath = '../../services/getDocInfo.xsjs';

var addDocPath = '../../services/addDoc.xsjs';
var downloadDocPath = '../../services/downloadDoc.xsjs';

var manageTables = {
  'SEARCHKEY':{
    bind:'/hana_srdl.tSearchKey',
    bindParameters: {expand: "REPORT"},
    title:'Голосовые команды',
    columns:[
      // {label:"ID фразы", bindValue: "ID", useSort: true, useFilter: true, width: "30%"},
      {label:"Голосовая команда", bindValue: "KEYWORD", useSort: true, useFilter: true, width: "40%"},
      {label:"Название отчета", bindValue: "REPORT/NAME", useSort: true, useFilter: true, width: "60%"},
    ]    
  },
  'REPORTTYPE':{
    bind:'/hana_srdl.tReport',
    bindParameters: {},
    title:'Отчеты',
    columns:[
      // {label:"ID отчета", bindValue: "ID", useSort: true, useFilter: true, width: "20%"},
      {label:"Название отчета", bindValue: "NAME", useSort: true, useFilter: true, width: "40%"},
      {label:"Ссылка", bindValue: "LINK", useSort: true, useFilter: true, width: "30%"},
      {label:"Описание отчета", bindValue: "DESCRIPTION", useSort: true, useFilter: true, width: "30%"},
    ]  
  },
  'DOC':{
    bind:'/hana_srdl.tDoc',
    bindParameters: {},
    title:'Документы',
    columns:[
      // {label:"ID документа", bindValue: "ID", useSort: true, useFilter: true, width: "30%"},
      {label:"имя документа", bindValue: "NAME", useSort: true, useFilter: true, width: "70%"},
      {label:"MIME", bindValue: "MIMECOLUMN", width: "30%"},
    ]  
  }
};


sap.ui.getCore().setModel(gMainJModel);



function send(url,success,error){
  $.ajax({ 
      type: 'GET', 
      url: url,
      success: success,
      error: error
  });
}

function setNotificationMessage(text){
    if (!useStatusBar)  gMainJModel.setData([{text:text}]); 
    else {
        gNotifier.removeAllMessages();
        gNotifier.addMessage(new sap.ui.core.Message({text:text}));        
    }
}

function clearNotificationMessage(){
    gNotifier.removeAllMessages();
    gMainJModel.setData([{text:""}]);
}


/*** Open Source jQuery Downloader from http://filamentgroup.com/lab/jquery_plugin_for_requesting_ajax_like_file_downloads/ ***/
jQuery.download = function(url, data, method){
    //url and data options required
    if( url && data ){ 
        //data can be string of parameters or array/object
        data = typeof data == 'string' ? data : jQuery.param(data);
        //split params into form inputs
        var inputs = '';
        jQuery.each(data.split('&'), function(){ 
//          var pair = this.split('=');
//          inputs+='<input type="hidden" name="'+ escape(pair[0]) +'" value="'+ escape(pair[1]) +'" />'; 
            var pair = this;
            inputs+='<input type="hidden" name="selected" value="'+ pair +'" />';             
        });
        //send request
        jQuery('<form action="'+ url +'" method="'+ (escape(method)||'post') +'">'+inputs+'</form>')
        .appendTo('body').submit().remove();
    };
};

function oDataGet(url,success,error){
  $.ajax({ 
      type: 'GET', 
      url: url,
      success: success,
      error: error
  });
}

function getODataToken(){
  $.ajax({ 
      type: 'GET', 
      url: oDataPath,
      success: function(data, textStatus, request){
        console.log(request.getResponseHeader('X-CSRF-TOKEN'));
      },
      beforeSend: function(xhr){xhr.setRequestHeader('X-CSRF-TOKEN', 'Fetch');},
      error: function(){setNotificationMessage("УПС... Внутренняя ошибка :(");console.log('Не удалось получить токен')}
  });
}

function oDataPOST(url,data,success,error){
  // получение токена
  $.ajax({ 
      type: 'GET', 
      url: oDataPath,
      success: function(response, textStatus, request){
        token = request.getResponseHeader('X-CSRF-TOKEN');
        $.ajax({
           url: url,
           data: data ? JSON.stringify(data) : undefined,
           type: "POST",
           processData: false,    //raw-data
           contentType: "application/x-www-form-urlencoded;charset=UTF-8",  //кодировка... жесть
           beforeSend: function(xhr){xhr.setRequestHeader('X-CSRF-TOKEN', token);},
           success: success,
           error: error
        });
      },
      beforeSend: function(xhr){xhr.setRequestHeader('X-CSRF-TOKEN', 'Fetch');},
      error: function(){setNotificationMessage("УПС... Внутренняя ошибка :(");console.log('Не удалось получить токен')}
  });
}




