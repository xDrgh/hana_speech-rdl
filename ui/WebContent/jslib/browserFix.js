if (!!sap.ui.Device.browser.webkit && !document.width) {

    jQuery.sap.require("sap.ui.core.ScrollBar");

     var fnOrg = sap.ui.core.ScrollBar.prototype.onAfterRendering;

     sap.ui.core.ScrollBar.prototype.onAfterRendering = function() {

         document.width = window.outerWidth;

         fnOrg.apply(this, arguments);

         document.width = undefined;

     };

}

if (!Document.prototype.createAttributeNS) {  
  Document.prototype.createAttributeNS = function(namespaceURI, qualifiedName) {  
   var dummy = this.createElement('dummy');  
   dummy.setAttributeNS(namespaceURI, qualifiedName, '');  
   var attr = dummy.attributes[0];  
   dummy.removeAttributeNode(attr);  
   return attr;  
  };  
}  
if (!Element.prototype.setAttributeNodeNS) {  
  Element.prototype.setAttributeNodeNS = Element.prototype.setAttributeNode;  
}  