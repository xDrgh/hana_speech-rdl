﻿// var gTemp,gTemp2;
var gJoke = false;

var gTabKeys = {
  'Поиск отчета':1,
  'tab 1':1,
  'таб 1':1,
  'вкладка 1':1,
  'Добавление команд':2,
  'tab 2':2,
  'таб 2':2,
  'вкладка 2':2,
  'Поиск по содержанию':3,
  'tab 3':3,
  'таб 3':3,
  'вкладка 3':3,
  'Загрузка документа':4,
  'tab 4':4,
  'таб 4':4,
  'вкладка 4':4,
  'Управление записями':5,
  'tab 5':5,
  'таб 5':5,
  'вкладка 5':5,
}
var gTab = FuzzySet(Object.keys(gTabKeys));
gTab.source = gTabKeys;

var gSpeechControlKeys = {  //не gState!!!
  'переключиться':0,
  'отчет':1,
  'введи':2,
  'телефон':4,
  'планшет':4,
  'наклони':3,
  'нагни':3,
  'поверни':3,
  'привет':5,
  'здравствуй':5,
  'лив':6,
  'live':6
}
var gSpeechControl = FuzzySet(Object.keys(gSpeechControlKeys));
gSpeechControl.source = gSpeechControlKeys;

var gRecognizingNow = false, // флаг идет ли распознование
	// gUseHana = false,	
	gHanaKey = ['хана','ханна','анна','hana','hanna','ханюша','анюша'],
  gYes = ['да','ага','ок','окей','окэй'],
  gNo = ['нет','неа','отказ'],
  gState = 0,                //0 - ждем ключевое слово (HANA), 1 - ждем команду (отчет, перекл...), 
  // 2 - название отчета, 3 - название таба для переключения,
  //4 - вводим содержимое док, 5 - ждем подтверждения, 
  // 6 - поворачиваем гугл; 7 - синхронизировать с телефоном
	gFinal_transcript = '';

var gRecognition;

var first_char = /\S/;
function capitalize(s) {
  return s.replace(first_char, function(m) { return m.toUpperCase(); });
}

// проверяем поддержку speach api
if (!('webkitSpeechRecognition' in window)) {

	//сделать недоступным иконку...  
	
	// start_button.style.display = "none";
	// showInfo("info_upgrade");
	
} else { /* инициализируем api */
 
 /* создаем объект 	*/
    gRecognition = new webkitSpeechRecognition();

    /* базовые настройки объекта */

    gRecognition.lang = 'ru'; // язык, который будет распозноваться. Значение - lang code
    gRecognition.continuous = true; // не хотим чтобы когда пользователь прикратил говорить, распознование закончилось
    gRecognition.interimResults = true;	//промежуточные

    /* метод вызывается когда начинается распознование */
    gRecognition.onstart = function() {
     
      gRecognizingNow = true;
      setNotificationMessage('Для того, чтобы использовать голосовой поиск, скажите "HANA", затем название отчета');
      gFinal_transcript = '';
      gState = 0;
      // showInfo('info_speak_now'); // меняем инфо текст
      // start_button.style.background = 'mic-animate.gif'; // меняем вид кнопки
    };
    
    /* обработчик ошибок */
    gRecognition.onerror = function(event) {
      if (event.error == 'no-speech') {
      	console.log('нет речи');
      }
      if (event.error == 'audio-capture') {
      	console.log('нет микрофона');
      }
    };
    
    /* метод вызывается когда распознование закончено */
    gRecognition.onend = function() {
     	gRecognizingNow = false;
    	setNotificationMessage('Распознование закончено. Для того, чтобы ввести голосовую команду - нажми "включить голосовое распознование"');
    };

    //ищет ключевое слово
    searchKeyWord = function(text){  	
      var arr = text.split(' ');    
    	for (var i =0; i< gHanaKey.length; i++){
        for (var j =0; j< arr.length; j++){
          if (arr[j] == gHanaKey[i])   return true; 
        }  
    	}

    	return false;
    }

  //подтверждение
  getApply = function(response){
    var last = -1;
    var arr = response.split(' ');  

    var key;  
    var i,j;
    for (j =0; j< arr.length; j++){
      for (i =0; i< gYes.length; i++){
        if (arr[j] == gYes[i]) { 
          gState = 0;    
          setNotificationMessage('HANA: Принято');      
          tryExexFuncApply();
          return 1;
        }
      }
      for (i =0; i< gNo.length; i++){
        if (arr[j] == gNo[i]) { 
          gState = 0;
          setNotificationMessage('Открытие отчета отменено');   
          return -1;
        }
      }

    }
    return 0;
  }

  //получение фразы, которая будет идти вслед за ключевым словом 'HANA'
  getPhrase4Recognition = function(text){
  	var last = -1;
    var arr = text.split(' ');  

    var key;  
    var i,j;
    for (j =0; j< arr.length; j++){
      for (i =0; i< gHanaKey.length; i++){
        if (arr[j] == gHanaKey[i]) {          
          last = j;
          key = gHanaKey[i];
          continue;
        }
      }  		
  	}
    
    if (last != -1) {      
      var index = text.lastIndexOf(key);
      index+=key.length;
      return text.substr(index);
  	}
    else return text;
  }
  //возвращает строкой параметры после определенного индекса
  getParameters = function(arr,index){
    if (index==arr.length) return "";
    var result = arr[index];
    for (var i = index+1;i<arr.length;i++)
      result+=' '+arr[i];
    return result;
  }
  //команда определена... выполняем ее
  runCommand = function(parameters){
    console.log(parameters);
    switch (gState){
      case 3:
        switchPage(parameters);
        return true;
      case 2:
        getReport(parameters);
        return true;
      case 4:
        enterText(parameters);
        return true;
      case 6:
        rotateGoogle();
        return true;
      case 7:
        syncReport(parameters);
        return true;
    }
  }
  //получение ключа
  getCommand = function(arr,key,tolerance){
    if (typeof tolerance == 'undefined')
      tolerance = 0.6;

    var out = arr.get(key);
    if (!out || !out.length) return undefined;
    for (var i =0;i < out.length; i++){
      if (out[i][0]>=tolerance)
        return arr.source[out[i][1]];
    }
    return undefined;
  }
  //поиск команды и параметров в тексте
  searchCommand = function(text){
    var arr = text.split(' ');
    var i;
    var result,parameters;
    for (i = 0; i<arr.length; i++){
      result = getCommand(gSpeechControl,arr[i]);
      if (typeof result == 'undefined') continue;

      switch (result){
        case 0:     //переключиться
          setNotificationMessage('Переключаемся на...');
          gState = 3;
          break;
        case 1:     //отчет
          setNotificationMessage('Отчет...');
          gState = 2;
          break;
        case 2:     //введи
          setNotificationMessage('Вводим...');
          gState = 4;
          break;
        case 3:
          setNotificationMessage('Поворачиваю...');
          gState = 6;
          break;
        case 4:
          setNotificationMessage('Синхронизирую с телефоном отчет...');
          gState = 7;
          break;
        case 5:
          setNotificationMessage('Привет!');
          gState = 0;
          return true;
        case 6:
          setNotificationMessage('Открываю HANA LIVE...');
          runHanaLive();
          return true;

      }
      parameters = getParameters(arr,i+1);      
      if (!parameters) continue;
      runCommand(parameters);
      return true;
    }
    return false;
    
  }

  /* 
    	метод вызывается после каждой сказанной фразы. Параметра event используем атрибуты:
  	- resultIndex - нижний индекс в результирующем массиве
  	- results - массив всех результатов в текущей сессии
   */
  gRecognition.onresult = function(event) {

  	var interim_transcript = '';
    for (var i = event.resultIndex; i < event.results.length; ++i) {
      if (event.results[i].isFinal) {  
      	if (gState == 1 || gState == 2 || gState == 3 || gState == 4 || gState == 7){	
        	gFinal_transcript += event.results[i][0].transcript;
    	}
        // console.info( event.results[i][0].transcript);        
      } else {      	
        interim_transcript += event.results[i][0].transcript;
        if (gState == 5) getApply(event.results[i][0].transcript);
      }
    }
    console.log('...' + interim_transcript);
    if (gState == 0)
      if (searchKeyWord(interim_transcript)) {gState = 1; setNotificationMessage('HANA: я Вас слушаю?');}

    if (gFinal_transcript) {
      var parameters;
      if (gState == 1){ //команда еще не определена
        parameters = getPhrase4Recognition(gFinal_transcript);
        if (parameters && !searchCommand(parameters)){
          gState = 0;
          if (gJoke)
            setNotificationMessage('Hana не понимать ;(');
          else
            setNotificationMessage('HANA: команда не распознана');
        }

      }
      else{
        runCommand(gFinal_transcript); //возможно не стоит бесконечно крутить!!!
      }

	  console.log('Final: '+parameters);     	 
	  gFinal_transcript='';
    }
      
  }

  function tryExexFuncApply(){
    try{
      gFuncAfterApply();
    } catch(e){
      console.log('Не удалось запустить функцию после подтверждения... что то пошло не так')
    }
    gFuncAfterApply = undefined;
  }

  function setReportType(result){
    console.info(result);  
    gReport = result.d.results;
    gState = 5;
    setNotificationMessage('Открыть отчет '+gReport.NAME+'?');
  }


  function requestError(result) {     
    gState = 0;
    setNotificationMessage('Ничего не найдено (');
  } 

  function getReport(params){  
    oDataPOST(oDataPath+getReportNamePath,{ "text": params },setReportType,requestError);
    gFuncAfterApply = openUrl;
  }

  function syncReport(params){
    oDataPOST(oDataPath+getReportNamePath,{ "text": params },setReportType,requestError);
    gFuncAfterApply = send4Sync;
  }

  function switchPage(params){
    var result = getCommand(gTab,params,0.4);
    gState = 0;
    if (typeof result == 'undefined') {
      setNotificationMessage("Неясно.. :(")
      return;
    }
    var oShell = sap.ui.getCore().byId("myShell");
    
    switch (result){
      case 1:     
          console.log(1);
          if (gPage==1) return;
          oShell.setSelectedWorksetItem("WI_REPORT");
          oShell.fireWorksetItemSelected({id:"WI_REPORT"});
          return;
      case 2:     
          console.log(2);
          if (gPage==2) return;
          oShell.setSelectedWorksetItem("WI_ADD_REPORT");
          oShell.fireWorksetItemSelected({id:"WI_ADD_REPORT"});
          return;
      case 3:     
          console.log(3);
          if (gPage==3) return;
          oShell.setSelectedWorksetItem("WI_DOC_SEARCH");
          oShell.fireWorksetItemSelected({id:"WI_DOC_SEARCH"});
          return;
      case 4:     
          console.log(4);
          if (gPage==4) return;
          oShell.setSelectedWorksetItem("WI_DOC_LOAD");
          oShell.fireWorksetItemSelected({id:"WI_DOC_LOAD"});
          return;
      case 5:     
          console.log(5);
          if (gPage==5) return;
          oShell.setSelectedWorksetItem("WI_M_1");
          oShell.fireWorksetItemSelected({id:"WI_M_1"});
          return;        
    }  
  }
  function detectStopWords(text){
    var arr = text.split(/(?:,| )+/),
        result = text;
    for (var i = 0; i<arr.length;i++){
      if (gStopList.indexOf(arr[i])!=-1 || arr[i].indexOf('*')!=-1){
        // result = result.replace(arr[i],"*");
        return true;
      }
    }
    return false;
  }

  function enterText(params){
    var temp = sap.ui.getCore();
    var oTxt = temp.byId("idTFSearch");
    var oShell = temp.byId("myShell");
    params = params.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
    if (detectStopWords(params)){
      setNotificationMessage('HANA: текст для ввода плохо распознан');
      return false;
    }
    oTxt.setValue(params);
    oTxt.fireLiveChange({liveValue:params});
    if (gPage!=3) { //переключиться 
      oShell.setSelectedWorksetItem("WI_DOC_SEARCH");    
      oShell.fireWorksetItemSelected({id:"WI_DOC_SEARCH"});
    }
    gState = 0;
    setNotificationMessage('HANA: ...');
  }

  function openUrl(){
    var params = gReport.LINK;
    setNotificationMessage('Открываю отчет. Что-нибудь еще?');
    var url = params;
    var params = [
      'height='+screen.height,
      'width='+screen.width,
      'fullscreen=yes' // only works in IE, but here for completeness
    ].join(',');
    var win=window.open(url, 'popup_window', params);
    // var win=window.open(url, '_blank');
    win.focus();
  }

  function send4Sync(){
    var url = oDataPath+"/hana_srdl.tReport('"+gReport.NAME+"')"+addDoc4MobilePath;
    oDataPOST(url);
    //сделать проверку...
    setNotificationMessage('Отправлено на устройство. Что-нибудь еще?');
  }

  function rotateGoogle(){
    var url = 'https://www.google.ru/search?q=tilt&oq=tilt&aqs=chrome.0.69i59j0l5.656j0j4&sourceid=chrome&espv=210&es_sm=93&ie=UTF-8';
    var params = [
      'height='+screen.height,
      'width='+screen.width,
      'fullscreen=yes' // only works in IE, but here for completeness
    ].join(',');
    var win=window.open(url, 'popup_window', params);
    win.focus();
    gState = 0;
    setNotificationMessage('HANA: кого еще?');
  }

  function runHanaLive(){
    var url = 'http://sap14:8000/sap/hba/explorer/index.html';
    var params = [
      'height='+screen.height,
      'width='+screen.width,
      'fullscreen=yes' // only works in IE, but here for completeness
    ].join(',');

    var win=window.open(url, 'popup_window', params);
    win.focus();
    // win..moveTo(0,0);
    gState = 0;
    setNotificationMessage('HANA: Что-нибудь еще?');
  }

  // /* переключаем состояния распознования
  function toogleRecognizing() {

    if (gRecognizingNow) { // если запись уже идет, тогда останавливаем
      gRecognition.stop();
      return;
    }
   
    gRecognition.start();
    
  }

  function startRecognizing(){
  	gRecognition.start();
  }

  function stopRecognizing(){
  	gRecognition.stop();
  }
}



