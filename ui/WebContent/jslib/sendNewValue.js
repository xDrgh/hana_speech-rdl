function addNewReport(reportName,ref,keyWords){
	if (!reportName || !ref)	setNotificationMessage('Не верно заданы параметры');	//вероятно вывести ошибку без кнопки применить
	if (!keyWords)	keyWords = reportName;

	var data = { "NAME": reportName, "LINK": ref, "WORDS": [] }
	var arr = keyWords.split(';');
	var word;
	for (i=arr.length-1;i>-1;i--){
        word = arr[i].replace(/^\s\s*/, '').replace(/\s\s*$/, '');
        if (word)
        	data.WORDS.push({"KEYWORD":word});
    }

	oDataPOST(addReportPath,data,sendSuccess,sendError);

	// var url = addReportPath+'?name='+reportName+'&ref='+ref+'&keys='+keyWords;
	// send(url,sendSuccess,sendError);
}

function sendSuccess(result){
  setNotificationMessage('Данные успешно добавлены');
}

function sendError(result) {     
	console.log('error');
	console.log(result);
	setNotificationMessage('Ошибка при добавлении. Данные не добавлены');
  // console.error('Ничего не найдено');
} 

// function send(url,success,error){
//   $.ajax({ 
//       type: 'GET', 
//       url: url,
//       success: success,
//       error: error
//   });
// }
