$.import("Konovalov.hana_srdl.libs","settings");
var Settings = $.Konovalov.hana_srdl.libs.settings.Settings;

var tableReportType = Settings.reportTypeTableName;
var tableReportKey = Settings.searchKeyTableName;
var seqRType = Settings.reportTypeSequence;
var seqRKey = Settings.searchKeySequence;

var body = '';

var reportName = $.request.parameters.get("name");
var keyWords = $.request.parameters.get("keys");
var ref = $.request.parameters.get("ref");
var description = $.request.parameters.get("des");
var additional = $.request.parameters.get("ad");    //скорее всего будет связано с гаджетом

var conn = $.db.getConnection(); 
//var conn = $.db.getConnection("Konovalov.hana_srdl.services::AnonConn");

var reportID;

//проверка на уникальность значения отчета
function checkReportType(reportName){
    try {
        var query = 'select top 1 * from '+tableReportType+' where "NAME" = ?';
        var st = conn.prepareStatement(query);  
        st.setString(1,reportName);
        var rs = st.executeQuery();
    }   catch(e){
        return false;
    }
    if (rs.next()) return false;
    return true;
}

function checkReportKey(key){
    try {
        var query = 'select top 1 * from '+tableReportKey+' where "KEYWORD" = ?';
        var st = conn.prepareStatement(query);  
        st.setString(1,key);
        var rs = st.executeQuery();
    }   catch(e){
        return false;
    }
    if (rs.next()) return false;
    return true;
}

//select * from "KONOVALOV"."speechReportSearchKey" where CONTAINS ("keyWord", 'оБоРот', EXACT);

function insertKey(text){
    var query = 'insert into '+tableReportKey+" values(?,?)";
    var st = conn.prepareStatement(query);
    st.setNString(1,text);
    st.setString(2,reportName);    
    var rs = st.execute();
}
function error(text,code){
    code = typeof code !== 'undefined' ? code: 400;
    $.response.setBody(text);  
    $.response.status = code;
    conn.close();
    return true;
}

function main(){
    var word;
    try {
        if (!reportName || !ref) {
            return error("parameters are not corrected");        
        }
        if (!checkReportType(reportName)) return error('Не удалось создать запись отчета с таким именем',406);
        if (typeof keyWords == 'undefined') keyWords = reportName;
        keyWords = keyWords.split(";");
        for (i=keyWords.length-1;i>-1;i--){
            word = keyWords[i].replace(/^\s\s*/, '').replace(/\s\s*$/, '');
            if (word){
                if (!checkReportKey(word)) return error('Не удалось создать запись отчета с таким ключевым словом',406);
                keyWords[i] = word;
            }
            else
                keyWords.splice(i, 1);
        }
    } catch (e){
        return error(e,400);
    }
    
    try {  
        var query = 'insert into '+tableReportType+" values(?,?,?,?)";
        var st = conn.prepareStatement(query);  
        
        st.setString(1,reportName);
        st.setNString(2,ref);
        st.setNString(3,description);
        st.setNString(4,additional);
        
        var rs = st.execute();
        
        for (var i = 0; i<keyWords.length; i++){
            insertKey(keyWords[i]);
        }
        
        body = "Записи добавлены";
        conn.commit();
        // $.response.contentType = 'plain/text';  
        $.response.setBody(body);  
        $.response.status = $.net.http.OK;  
    } catch (e) {  
        $.response.setBody(e);  
        $.response.status = 500;  
        }  
    conn.close();  
    return true;
}

main();

