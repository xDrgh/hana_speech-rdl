$.import("Konovalov.hana_srdl.libs","settings");
var Settings = $.Konovalov.hana_srdl.libs.settings.Settings;

var tableName = Settings.view1;

var conn = $.db.getConnection(); 
// var conn = $.db.getConnection("Konovalov.hana_srdl.services::AnonConn");

function getReport(text){
    var query = 'select top 1 "NAME", "LINK", "ADDITIONAL" FROM '+tableName;
    query+=' WHERE CONTAINS("KEYWORD",?,';
    query+=" FUZZY(0.7, 'textSearch=fulltext'))";
    query+=' ORDER BY score()';
    
    var st = conn.prepareStatement(query);  
    st.setString(1,text);
    var rs = st.executeQuery();
    if (!rs.next()) return false;
    var reportType = {};
    reportType.name = rs.getString(1);
    reportType.view = rs.getString(2);
    reportType.additional = rs.getString(3);
    return reportType;
}

var body = '';
var reportType;
var index;
var parameters = [];

var text = $.request.parameters.get("text_for_search");

try {  
    if (!text) {
        throw("Parameter 'text_for_search' does not exist!");        
    }
    reportType = getReport(text);
    //пробуем убрать предлоги вначале если они есть 
    if (!reportType) {
        var arr = text.split(' ');
        if (arr.length > 1) {
            if (arr[0].length < 3) {
                text = text.substr(arr[0].length);
                reportType = getReport(text);
            }
        }
    }
    //если не найден - уменьшает по одному слову пока не найдет, или пока не дойдет до одного слово
    //увеличивает шанс распознования (минимум берет 2 слова)
    while (!reportType){
        index = text.lastIndexOf(' ');
        text = text.substr(0,index);
        if (text.split(' ').length == 0) throw 'no results';
        reportType = getReport(text);
    }
    
    body = reportType;
    
//    $.response.headers.set("Content-Disposition", "Content-Disposition: attachment; filename=bts201002.pdf");  
    $.response.contentType = 'application/json';  
    $.response.setBody(JSON.stringify(body));  
    $.response.status = $.net.http.OK;  
} catch (e) {  
    $.response.setBody("Error while downloading : "+e);  
    $.response.status = 500;  
    }  
conn.close();  

