$.import("Konovalov.hana_srdl.libs","settings");
var Settings = $.Konovalov.hana_srdl.libs.settings.Settings;
var tableName = Settings.mobileTableName;
var tableReportType = Settings.reportTypeTableName;

var conn = $.db.getConnection(); 
// var conn = $.db.getConnection("Konovalov.hana_srdl.services::AnonConn");
var userName = $.session.getUsername();

var body = '', recordID, reportID, reportName, reportURL;

var recordID = $.request.parameters.get("id");
var status = $.request.parameters.get("status");

function checkDoc(id){
    var result = {};
    var query = 'select top 1 "ID" from '+tableName+' where "STATE" = ? AND "LOGIN" = ? AND "ID" = ?';
    var st = conn.prepareStatement(query);
    st.setInteger(1,1); //только отправленные
    st.setNString(2,userName);
    st.setInteger(3,id);
    var rs = st.executeQuery();
    if (!rs.next()) return false;
    return true;
}

//проставляем статус отправляемой записи
function updateRecord(recordID,status){
    var result = {};
    var query = 'UPDATE '+tableName+' SET "STATE" = ? WHERE "ID" = ?';
    var st = conn.prepareStatement(query);
    st.setInteger(1,status);
    st.setInteger(2,recordID);
    var rs = st.execute();
    conn.commit();  
    return true;
}

function error(text,code){
    code = typeof code !== 'undefined' ? code: 400;
    $.response.setBody(text);  
    $.response.status = code;
    return true;
}
function empty(){
    $.response.status = 204; //$.net.http.NOCONTENT;  
    conn.close();
    return true;
}

function success(body){
    $.response.setBody(body);  
    // $.response.contentType = "application/json"; 
    $.response.status = $.net.http.OK;  
    conn.close();
    return true;
}

function main(){
    var result = {};
    if (!recordID || !status) return error("Не заданы параметры");
    status = Number(status);
    if (!(status==2 || status==3)) return error("Невозможно задать данный статус");
    recordID = Number(recordID);
    if (!checkDoc(recordID)) return error("Искомый документ не найден");

    try {  
        
        updateRecord(recordID, status);
        success("Принято");

    } catch (e) {  
        error(e,500);
    } 
    return true;
}

main();