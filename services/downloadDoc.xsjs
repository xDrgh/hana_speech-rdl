$.import("Konovalov.hana_srdl.libs","settings");
var Settings = $.Konovalov.hana_srdl.libs.settings.Settings;
var tableName = Settings.docTableName;

var conn = $.db.getConnection(); 
//var conn = $.db.getConnection("Konovalov.hana_srdl.services::AnonConn");
var name = $.request.parameters.get("name")

//обоход падения при обращении к несуществующей записи
function isInList(name) {
    var query = 'select "NAME" FROM '+tableName;
    var rs = conn.prepareStatement(query).executeQuery();
    while (rs.next()) {
        return true;
    }
    return false;
}

try {  
    if (!name) 
        throw new ReferenceError("Parameters are incorrect");
    if (!isInList(name)) {
        throw new ReferenceError("Document doesn't exist");        
    }
    var query = 'select "NAME","DOC","MIMECOLUMN" FROM '+tableName+' where';
    query+= '"NAME" = ?';
    var st = conn.prepareStatement(query);
    st.setNString(1,name);
    var rs = st.executeQuery();
    rs.next();  //проверка на сущестовование выполнена ранее
    
    var filename = rs.getNString(1);
    var mimeType = rs.getNString(3);
    $.response.headers.set("Content-Disposition", "Content-Disposition: attachment; filename="+filename);  
    $.response.contentType = mimeType;  
    $.response.setBody(rs.getBlob(2));  
    $.response.status = $.net.http.OK;  
} catch (e) {  
    $.response.setBody("Error while downloading : "+e);  
    $.response.status = 500;  
    }  
conn.close();  
