$.import("Konovalov.hana_srdl.libs","settings");
var Settings = $.Konovalov.hana_srdl.libs.settings.Settings;
var tableName = Settings.mobileTableName;
var sequence = Settings.mobileSequence;
var tableReportType = Settings.reportTypeTableName;

var conn = $.db.getConnection(); 
//var conn = $.db.getConnection("Konovalov.hana_srdl.services::AnonConn");
var userName = $.session.getUsername();

var body = '', recordID, timestamp;

var reportID, reportName = $.request.parameters.get("name");

//запись _серверного_ времени
function getTimestamp(){
    var query = 'select now() from dummy';
    var rs = conn.prepareStatement(query).executeQuery();
    if (!rs.next()) throw 'error in getTimeStamp...how???';
    var timestamp = rs.getTimestamp(1);
    return timestamp;
}

//проверка на присутствие данного отчета
function checkReport(name){
    try {
        var query = 'select "NAME" from '+tableReportType+' where "NAME" = ?';
        var st = conn.prepareStatement(query);  
        st.setString(1,name);
        var rs = st.executeQuery();
    }   catch(e){
        return false;
    }
    if (rs.next()) return true;
    return false;
}

function error(text,code){
    code = typeof code !== 'undefined' ? code: 400;
    $.response.setBody(text);  
    $.response.status = code;
    return true;
}

function main(){
    var i;
    if (!reportName) 
        return error("parameters are not corrected");   

    if (!checkReport(reportName)) 
        return error("this report doesn't exist");   
    
    try {  
        var query = 'insert into '+tableName+" values(?,?,?,?)";
        var st = conn.prepareStatement(query);  
        
        timestamp = getTimestamp();
        
        st.setNString(1,userName);
        st.setTimestamp(2,timestamp);
        st.setNString(3,reportName);
        st.setInteger(4,0);     //запись создана, но не запрошена устройством
        
        var rs = st.execute();
        
        body = "Record added";
        conn.commit();
        $.response.setBody(body);  
        $.response.status = $.net.http.OK;  
    } catch (e) {  
        $.response.setBody(e);  
        $.response.status = 500;  
        }  
    conn.close();  
    return true;
}

main();