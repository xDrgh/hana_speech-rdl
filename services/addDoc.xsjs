$.import("Konovalov.hana_srdl.libs","settings");
var Settings = $.Konovalov.hana_srdl.libs.settings.Settings;
var tableName = Settings.docTableName;
var columnNameLimit = Settings.docNameLimit;
var sequence = Settings.docSequence;

var conn = $.db.getConnection(); 
// var conn = $.db.getConnection("Konovalov.hana_speech.services::AnonConn");

//извлечение содержимого и названия файла
function getEntityContentName(entity) {  
    if (!entity.headers) return null;
    var result = {};
    for (var i = 0; i < entity.headers.length; i++) {  
        if (entity.headers[i].name && (entity.headers[i].name === "~content_name"))  {
            result.content = entity.headers[i].value;
        } else if (entity.headers[i].name && (entity.headers[i].name === "~content_filename")){
            result.name = entity.headers[i].value;
        }
    }  
    if (result.content) return result;
    return null;  
}  

//загрузка файла в таблицу
function loadData(file){
    if (typeof file.name == 'undefined') return false;
    var name = file.name.substring(0,columnNameLimit);  
    var query = 'insert into '+tableName+' values(?,?,?)';    
    var mimeType = file.mimeType;
    try{
    	var st = conn.prepareStatement(query); 
    	st.setString(1,name);  
        st.setBlob(2,file.contents);  
        st.setString(3,mimeType);  
    	st.execute();  
    	conn.commit();  
    } catch(e){
        $.response.setBody("Error while inserting into the table : "+e);  
        $.response.status = 500;
        return false;
    }  
    $.response.setBody("200");  //на стороне клиента странные методы обработки результата...
    $.response.status = $.net.http.OK;
    return true;
    
}
function extractMultiPartParameters(entities) {   
    for (var i = 0; i < entities.length; i++) {  
        var contentName = getEntityContentName(entities[i]);  
        if (contentName) {  
            var file = {};  
            //проверка типа!!!
            file.mimeType = entities[i].contentType;  
            file.contents = entities[i].body.asArrayBuffer(); 
            file.name = contentName.name;
            if (!loadData(file)) return false; 
        } 
    }
	conn.close();  
	return true;
}  

extractMultiPartParameters($.request.entities);