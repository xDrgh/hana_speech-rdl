$.import("Konovalov.hana_srdl.libs","settings");
var Settings = $.Konovalov.hana_srdl.libs.settings.Settings;
var tableName = Settings.mobileTableName;
var tableReportType = Settings.reportTypeTableName;

var conn = $.db.getConnection(); 
// var conn = $.db.getConnection("Konovalov.hana_srdl.services::AnonConn");
var userName = $.session.getUsername();

var body = '', recordID, reportID, reportName, reportURL;

function getFirstRecord(){
    var result = {};
    var query = 'select top 1 "REPORT", "DATE" from '+tableName+' where ("STATE" = ? OR "STATE" = ?) AND "LOGIN" = ? order by "DATE"';
    var st = conn.prepareStatement(query);
    st.setInteger(1,0);
    st.setInteger(2,1);
    st.setNString(3,userName);
    var rs = st.executeQuery();
    if (!rs.next()) return false;
    result.report = rs.getNString(1);
    result.date = rs.getTimestamp(2);
    return result;
}

function getReport(reportID){
    var query = 'select "NAME", "link", "additional" FROM '+tableReportType+' where "ID" = ?';    
    var st = conn.prepareStatement(query);  
    st.setInteger(1,reportID);
    var rs = st.executeQuery();
    if (!rs.next()) return false;
    var reportType = {};
    reportType.name = rs.getNString(1);
    reportType.link = rs.getNString(2);
    reportType.additional = rs.getNString(3);
    return reportType;
}

//проставляем статус отправляемой записи
function updateRecord(date){
    var result = {};
    var query = 'UPDATE '+tableName+' SET "STATE" = ? WHERE "DATE" = ?';    //лучше указать и пользователя для ускорения, но сейчас игнор
    var st = conn.prepareStatement(query);
    st.setInteger(1,1);
    st.setTimestamp(2,date);
    var rs = st.execute();
    conn.commit();  
    return true;
}

function error(text,code){
    code = typeof code !== 'undefined' ? code: 400;
    $.response.setBody(text);  
    $.response.status = code;
    return true;
}
function empty(){
    $.response.status = 204; //$.net.http.NOCONTENT;  
    conn.close();
    return true;
}

function success(body){
    $.response.setBody(body);  
    // $.response.contentType = "application/json"; 
    $.response.status = $.net.http.OK;  
    conn.close();
    return true;
}

function main(){
    var result = {};
    
    try {  
        var recordInfo = getFirstRecord();
        if (!recordInfo) return empty();  //нет данных для передачи
        updateRecord(recordInfo.date);
        var reportInfo = getReport(recordInfo.report);
        if (!reportInfo) return empty();  //произошла ошибка, отчет был удален (предположительно)... ничего не вернем ;)
        result.date = recordInfo.date;
        result.name = recordInfo.report;
        result.link = reportInfo.link;
        result.additional = reportInfo.additional;
        success(JSON.stringify(result));

    } catch (e) {  
        error(e,500);
    } 
    return true;
}

main();