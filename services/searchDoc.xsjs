$.import("Konovalov.hana_srdl.libs","settings");
var Settings = $.Konovalov.hana_srdl.libs.settings.Settings;
var tableName = Settings.docTableName;

var text = $.request.parameters.get("s");
var useExact = $.request.parameters.get("exact");
var conn = $.db.getConnection(); 
//var conn = $.db.getConnection("Konovalov.hana_speech.services::AnonConn");

function getContenxt(rs,index){
    try{
        return rs.getNString(index);
    }
    catch(e){
        return rs.getNClob(index);
    }

}

try {  
    if (!text) {
        throw new ReferenceError("Parameter 's' does not exist!");  
    }
    text = '"'+text.toString()+'"';

    // var query = 'select ID,TO_DECIMAL(SCORE(),3,2) AS score, "NAME", HIGHLIGHTED("DOC") FROM '+tableName;
    if (useExact){
        var query = 'select TO_DECIMAL(SCORE(),3,2) AS score, "NAME", SNIPPETS("DOC") FROM '+tableName;
        query+="WHERE CONTAINS(\"DOC\",?, EXACT)";
        query+='ORDER BY score DESC';
    }
    else{
        var query = 'select TO_DECIMAL(SCORE(),3,2) AS score, "NAME", SNIPPETS("DOC") FROM '+tableName;
        query+="WHERE CONTAINS(\"DOC\",?, FUZZY(0.5, 'textSearch=fulltext'))";
        query+='ORDER BY score DESC';
    }

    // $.response.setBody(query);
    var st = conn.prepareStatement(query);  
    st.setNString(1,text);
    var rs = st.executeQuery();
    var body = {result:[]};
    var context;

    while (rs.next()){
        context = getContenxt(rs,3);
        body.result.push({score:rs.getString(1), name:rs.getString(2), context:context});
    }

    $.response.contentType = 'application/json';  
    $.response.setBody(JSON.stringify(body));  
    $.response.status = $.net.http.OK;  
} catch (e) {  
    $.response.setBody("Error : "+e);  
    $.response.status = 500;  
    }  

conn.close();  



