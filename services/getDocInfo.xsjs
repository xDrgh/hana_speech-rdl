$.import("Konovalov.hana_srdl.libs","settings");
var Settings = $.Konovalov.hana_srdl.libs.settings.Settings;
var tableName = Settings.docFullTextIndex;

var type = $.request.parameters.get("type");
var name = $.request.parameters.get("name");
var conn = $.db.getConnection(); 
//var conn = $.db.getConnection("Konovalov.hana_speech.services::AnonConn");

try {  
    if (!type || !name) {
        throw new ReferenceError("Parameters are incorrect");  
    }
    type = type.toString();
    var query = 'select distinct "TA_TOKEN" from '+tableName;
    query+='where "NAME" = ? AND "TA_TYPE" = ?';
    var st = conn.prepareStatement(query);  
    st.setString(1,name);
    st.setString(2,type);
    var rs = st.executeQuery();
    var body = {result:[]};
    var context;

    while (rs.next()){
        body.result.push({word: rs.getNString(1)});
    }

    $.response.contentType = 'application/json';  
    $.response.setBody(JSON.stringify(body));  
    $.response.status = $.net.http.OK;  
} catch (e) {  
    $.response.setBody("Error : "+e);  
    $.response.status = 500;  
}  

conn.close();  







